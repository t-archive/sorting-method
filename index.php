<?php
require_once("SortingAlgorithm.php");


$numbers = [];
for ($i = 0; $i < 30; $i++) {
    $numbers[] = round(rand(10, 99));
}

$sorted = SortingAlgorithm::bubble($numbers);
print_r($sorted);
echo "<hr>";
$sorted = SortingAlgorithm::myBubble($numbers);
print_r($sorted);
echo "<hr>";
$sorted = SortingAlgorithm::mySort($numbers);
print_r($sorted);
echo "<hr>";
echo "Bubble: ";
echo SortingAlgorithm::$countBubble;
echo "<br>MyBubble: ";
echo SortingAlgorithm::$countMyBubble;
echo "<br>MySort: ";
echo SortingAlgorithm::$countMySort;
