<?php


class SortingAlgorithm
{
    public static $countBubble = 0;
    public static $countMyBubble = 0;
    public static $countMySort = 0;


    public static function bubble($content)
    {
        for ($outCircle = 0; $outCircle < count($content); $outCircle++) {
            for ($inCircle = 0; $inCircle < count($content) - 1; $inCircle++) {
                SortingAlgorithm::$countBubble++;
                if ($content[$inCircle + 1] < $content[$inCircle]) {
                    $temp = $content[$inCircle];
                    $content[$inCircle] = $content[$inCircle + 1];
                    $content[$inCircle + 1] = $temp;
                }
            }
        }
        return $content;
    }

    public static function bubbleForMyBubble($content)
    {
        for ($outCircle = 0; $outCircle < count($content); $outCircle++) {
            for ($inCircle = 0; $inCircle < count($content) - 1; $inCircle++) {
                SortingAlgorithm::$countMyBubble++;
                if ($content[$inCircle + 1] < $content[$inCircle]) {
                    $temp = $content[$inCircle];
                    $content[$inCircle] = $content[$inCircle + 1];
                    $content[$inCircle + 1] = $temp;
                }
            }
        }
        return $content;
    }
    public static function myBubble($content = [])
    {
        if (count($content) <= 1) {
            return $content;
        }
        $array = array_chunk($content, round(count($content) / 2));
        $resultOne = SortingAlgorithm::bubbleForMyBubble($array[0]);
        $resultTwo = SortingAlgorithm::bubbleForMyBubble($array[1]);
        return SortingAlgorithm::interlinkSortArraysMyBubble($resultOne, $resultTwo);
    }

    public static function mySort($content = [])
    {
        if (count($content) <= 1) {
            return $content;
        }
        $array = array_chunk($content, round(count($content) / 2));
        $resultOne = SortingAlgorithm::mySort($array[0]);
        $resultTwo = SortingAlgorithm::mySort($array[1]);
        return SortingAlgorithm::interlinkSortArraysMySort($resultOne, $resultTwo);
    }

    private static function interlinkSortArraysMyBubble($firstArray, $secArray)
    {
        $i = 0;
        $j = 0;
        $result = [];
        while ($i + $j < count($firstArray) + count($secArray)) {
            SortingAlgorithm::$countMyBubble++;
            if (!array_key_exists($i, $firstArray)) {
                $result = array_merge($result, array_slice($secArray, $j));
                break;
            }

            if (!array_key_exists($j, $secArray)) {
                $result = array_merge($result, array_slice($firstArray, $i));
                break;
            }

            $first = $firstArray[$i];
            $sec = $secArray[$j];

            if ($first < $sec) {
                $i++;
                $result[] = $first;
            } else {
                $j++;
                $result[] = $sec;
            }
        }
        return $result;

    }
    private static function interlinkSortArraysMySort($firstArray, $secArray)
    {
        $i = 0;
        $j = 0;
        $result = [];
        while ($i + $j < count($firstArray) + count($secArray)) {
            SortingAlgorithm::$countMySort++;
            if (!array_key_exists($i, $firstArray)) {
                $result = array_merge($result, array_slice($secArray, $j));
                break;
            }

            if (!array_key_exists($j, $secArray)) {
                $result = array_merge($result, array_slice($firstArray, $i));
                break;
            }

            $first = $firstArray[$i];
            $sec = $secArray[$j];

            if ($first < $sec) {
                $i++;
                $result[] = $first;
            } else {
                $j++;
                $result[] = $sec;
            }
        }
        return $result;

    }
}
